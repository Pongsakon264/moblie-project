import 'package:flutter/material.dart';

class ItemScreen3 extends StatelessWidget {
  const ItemScreen3({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Rack Pulls Deadlift'),backgroundColor: Colors.green
      ),
      body: ListView(
        padding: const EdgeInsets.all(32) ,
        children: [
          Image.asset(
            'images/deadlift.jpg',
            width: 400,
            height: 240,
            fit: BoxFit.contain,
          ),
         
          Row(
            children: [
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      'วิธีการเล่น Rack Pulls Deadlift อย่างถูกวิธี',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    'From menfitness',
                    style: TextStyle(color: Colors.grey[500]),
                  )
                ],
              )),
              Icon(
                Icons.star_border_purple500,
                color: Colors.red[600],
              ),
              Text('46')
            ],
          ),
          Text(
       'เริ่มฝึกท่า Rack Pulls ด้วยการจัดร่างกายให้อยู่ในท่าเตรียม Deadlift แล้วยกบาร์เบลขึ้นเหนือหัวเข่าเล็กน้อย จนขาทั้งสองข้างเหยียดตรง '
        'ซึ่งท่านี้จะช่วยสร้างความแข็งแรงให้กับกล้ามเนื้อหลังส่วนบนได้เป็นอย่างดี รวมถึง แขน ไหล่ และสะโพกด้วย',
        softWrap: true,
      ),
        ],
      ),
    );
  }
}
