import 'package:flutter/material.dart';

class ItemScreen1 extends StatelessWidget {
  const ItemScreen1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Brench Press'),backgroundColor: Colors.green,
      ),
      body: ListView(
        padding: const EdgeInsets.all(32) ,
        children: [
          Image.asset(
            'images/barbell-bench-press-start.jpg',
            width: 400,
            height: 240,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/barbell-bench-press-finish.jpg',
            width: 400,
            height: 240,
            fit: BoxFit.contain,
          ),
          Row(
            children: [
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      'วิธีการเล่น Brench Press อย่างถูกวิธี',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    'From Naefit',
                    style: TextStyle(color: Colors.grey[500]),
                  )
                ],
              )),
              Icon(
                Icons.star_border_purple500,
                color: Colors.red[600],
              ),
              Text('100')
            ],
          ),
          Text(
        'การฝึกด้วยท่า Barbell Bench Press เริ่มต้นจากการนอนหงายหน้าลงบนเบาะ เท้าวางสนิทติดกับพื้น หงายมือทั้งสองข้างขึ้น จับบาร์เบลด้วยความกว้างกว่าหัวไหล่เล็กน้อย '
        'จากนั้นยก บาร์เบลออกมาจากที่พัก ในลักษณะที่แขนตึง แต่ไม่ล็อกข้อศอก เป็นท่าเตรียมฝึกท่า Barbell Bench Press '

        '1.ค่อยๆคลายกล้ามเนื้อหน้าอก ออก งอแขน ลดบาร์เบลลงจนแตะยอดอก โดยให้แขนทำมุมประมาณ 70 องศากับลำตัว พร้อมกับสูดลมหายใจเข้าจนสุด '

        '2.จากนั้นเริ่มออกแรงเกร็งกล้ามเนื้อหน้าอกเพื่อยกบาร์เบลขึ้นจนสุดพิสัย เพื่อกลับสู่ท่าเตรียม พร้อมกับปล่อยลมหายใจออกจนสุด นับเป็น 1 ครั้ง',
        softWrap: true,
      ),
        ],
      ),
    );
  }
}
