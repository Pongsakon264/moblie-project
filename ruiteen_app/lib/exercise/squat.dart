import 'package:flutter/material.dart';

class ItemScreen4 extends StatelessWidget {
  const ItemScreen4({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Barbell Squat'),backgroundColor: Colors.green
      ),
      body: ListView(
        padding: const EdgeInsets.all(32) ,
        children: [
          Image.asset(
            'images/barbell-squat-start-resize-1-1.jpg',
            width: 400,
            height: 240,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/barbell-squat-finish-resize-1-1.jpg',
            width: 400,
            height: 240,
            fit: BoxFit.contain,
          ),
          Row(
            children: [
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      'วิธีการเล่น Barbell Squat อย่างถูกวิธี',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    'From Planforfit',
                    style: TextStyle(color: Colors.grey[500]),
                  )
                ],
              )),
              Icon(
                Icons.star_border_purple500,
                color: Colors.red[600],
              ),
              Text('120')
            ],
          ),
          Text(
        'วิธีการฝึก Barbell Squat ให้ผู้ฝึกเซต power rack ให้บาร์เบลอยู่สูงเท่ากับส่วนอกของผู้ฝึก '
        '1.ให้ผู้ฝึกยืนอยู่ตรงกลางบาร์เบลและยกบาร์เบลโดยที่ย่อตัวลงไปใต้บาร์เบลและวางบาร์เบลไว้ตรงหลังส่วนบกตรงสะบักและประคองบาร์เบลด้วยมือทั้ง2ข้างและยกบาร์เบลออกจาก power rack ด้วยถ้ายืนที่ขาทั้ง2ข้างของผู้ฝึกกว้างพอดีกับหัวไหล่และหลักตั้งฉากกับพื้นพร้อมสุดลมหายใจเข้าจดสุดเป็นท่าเตรียม '
         
        '2.ให้ผู้ฝึกค่อยๆนั่งย่อตัวลงจนต้นขาทำแนวระนาบขนานกับพื้นโลกโดยที่หย่อนก้นลงก่อนคล้ายกับนั่งเก้าอี้พร้อมกับสูดลมหายใจเข้า '
        
        '3.ออกแรงโดยที่ดันบาร์ขึ้นด้วยหน้าขาจนขาและลำตัวของผู้ฝึกตั้งฉากกลับพื้นเพื่อกลับมาสู่ท่าเตรียมพร้อมกับปล่อยลมหายใจออก นับเป็น1ครั้ง '
        '***ควรใส่เข็มขัดบล็อคหลังระหว่างการฝึกเพื่อป้องกันการเจ็บหลังส่วนล่าง***',
        softWrap: true,
      ),
        ],
      ),
    );
  }
}
