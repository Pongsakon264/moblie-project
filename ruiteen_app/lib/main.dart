import 'package:flutter/material.dart';
import 'package:ruiteen_app/exercise/brenchpress.dart';
import 'package:ruiteen_app/exercise/deadlift.dart';
import 'package:ruiteen_app/exercise/shoulderpress.dart';
import 'package:ruiteen_app/exercise/squat.dart';
import 'package:ruiteen_app/food.dart';
import 'package:ruiteen_app/Routine.dart';
import 'package:ruiteen_app/login/home_page.dart';
import 'package:ruiteen_app/login/login.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
   final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    HomePage.tag: (context) => HomePage(),
  };
  // const MyApp({Key? key}) : super(key: key);
  // static const appTitle = 'Today Ruiteen';
  @override
    Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Login UI',
      theme: ThemeData(
        primarySwatch: Colors.lightBlue,
        fontFamily: 'Nunito',
      ),
      home: LoginPage(),
      routes: routes,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState(title: title);
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState({required this.title});
  final String title;
  Widget body = Center(
    child: Text('My Page!'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: 
      AppBar(title: Text(title),backgroundColor: Colors.green,),
      
      
      
      drawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Text('TODAY RUITEEN'),
              decoration: BoxDecoration(
                color: Colors.green,
              ),
            ),
            ListTile(
              title: const Text('Brench press'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => ItemScreen1()));
                
              },
            ),
            ListTile(
              title: const Text('Shoulder press'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => ItemScreen2()));
              },
            ),
            ListTile(
              title: const Text('Deadlift'),
              onTap: () {
                Navigator.push(context, MaterialPageRoute(builder: (context) => ItemScreen3()));
              },
            ),
            ListTile(
              title: const Text('Squat'),
              onTap: () {
                 Navigator.push(context, MaterialPageRoute(builder: (context) => ItemScreen4()));
              },
            ),
          ],
        ),
      ),
      
      body: ListView(children: [Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Routine()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/img1.png?alt=media&token=dcb15513-9686-4e48-b0d7-6d170d225667'))),
                ),
              ),
            ),Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => Food()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/fd1.jpg?alt=media&token=823f1af6-fbea-4a80-a502-42fe619996d1'))),
                ),
              ),
            )],)
            
    );
  }
}