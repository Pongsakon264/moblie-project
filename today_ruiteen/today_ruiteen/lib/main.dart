import 'package:flutter/material.dart';
import 'package:today_ruiteen/exercise/exercise.dart';
import 'package:today_ruiteen/profile/profile.dart';
import 'package:today_ruiteen/exercise/brenchpress.dart';
import 'package:today_ruiteen/exercise/deadlift.dart';
import 'package:today_ruiteen/exercise/shoulderpress.dart';
import 'package:today_ruiteen/exercise/squat.dart';
import 'package:today_ruiteen/food/food.dart';
import 'package:today_ruiteen/login/home_page.dart';
import 'package:today_ruiteen/login/login.dart';
import 'package:today_ruiteen/routine/routine.dart';
import 'package:today_ruiteen/routine/routine_app_widget.dart';
import 'package:today_ruiteen/routine/routine_from_widget.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  final routes = <String, WidgetBuilder>{
    LoginPage.tag: (context) => LoginPage(),
    HomePage.tag: (context) => HomePage(),
  };
  // const MyApp({Key? key}) : super(key: key);
  // static const appTitle = 'Today Ruiteen';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Today Ruitine',
      theme: ThemeData(
        primarySwatch: Colors.green,
        fontFamily: 'Nunito',
      ),
      home: LoginPage(),
      routes: routes,
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key? key, required this.title}) : super(key: key);
  final String title;
  @override
  _MyHomePageState createState() => _MyHomePageState(title: title);
}

class _MyHomePageState extends State<MyHomePage> {
  _MyHomePageState({required this.title});
  final String title;
  Widget body = Center(
    child: Text('My Page!'),
  );
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
          backgroundColor: Colors.green,
        ),
        drawer: Drawer(
            elevation: 1.5,
            child: Column(children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  
                  color: Colors.green[200],
                ), child: null,
                
              ),
              Expanded(
                  child: ListView(
                padding: EdgeInsets.zero,
                children: <Widget>[
                  ListTile(
                    title: Text('Your Profile'),
                    leading: Icon(Icons.person),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfilePage()));
                    },
                  ),
                 
                 
                  ListTile(
                    title: Text('Log out'),
                    leading: Icon(Icons.logout),
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => LoginPage()));
                    },
                  )
                ],
              )),
              Container(
                color: Colors.black,
                width: double.infinity,
                height: 0.1,
              ),
              Container(
                  padding: EdgeInsets.all(10),
                  height: 100,
                  child: Text(
                    "V1.0.0",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  )),
            ])),

        // drawer: Drawer(
        //   child: ListView(
        //     padding: EdgeInsets.zero,
        //     children: [
        //       DrawerHeader(
        //         child: Text('TODAY RUITEEN'),
        //         decoration: BoxDecoration(
        //           color: Colors.green,
        //         ),
        //       ),
        //       ListTile(
        //         title: const Text('Brench press'),
        //         onTap: () {
        //           Navigator.push(context, MaterialPageRoute(builder: (context) => ItemScreen1()));

        //         },
        //       ),
        //       ListTile(
        //         title: const Text('Shoulder press'),
        //         onTap: () {
        //           Navigator.push(context, MaterialPageRoute(builder: (context) => ItemScreen2()));
        //         },
        //       ),
        //       ListTile(
        //         title: const Text('Deadlift'),
        //         onTap: () {
        //           Navigator.push(context, MaterialPageRoute(builder: (context) => ItemScreen3()));
        //         },
        //       ),
        //       ListTile(
        //         title: const Text('Squat'),
        //         onTap: () {
        //            Navigator.push(context, MaterialPageRoute(builder: (context) => ItemScreen4()));
        //         },
        //       ),
        //     ],
        //   ),
        // ),

        body: ListView(
          children: [
            Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => DogApp()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/img1.png?alt=media&token=dcb15513-9686-4e48-b0d7-6d170d225667'))),
                ),
              ),
            ),
            Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => Food()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/fd1.jpg?alt=media&token=823f1af6-fbea-4a80-a502-42fe619996d1'))),
                ),
              ),
            ),
             Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => exercise()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://firebasestorage.googleapis.com/v0/b/cookbook-bb297.appspot.com/o/p%2FExercise2.jpg?alt=media&token=066e1e5e-3559-4b2d-8bee-18cc531f006d'))),
                ),
              ),
            )
          ],
        ));
  }
}
