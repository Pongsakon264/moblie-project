import 'package:flutter/material.dart';

class clean1 extends StatelessWidget {
  const clean1({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('สลัดฟักทองย่างเคลและถั่วเลนทิล'),backgroundColor: Colors.green
      ),
      body: ListView(
        padding: const EdgeInsets.all(32) ,
        children: [
          Image.asset(
            'images/clean1.jpg',
            width: 400,
            height: 240,
            fit: BoxFit.contain,
          ),
          Image.asset(
            'images/cleanD1.jpg',
            width: 400,
            height: 240,
            fit: BoxFit.contain,
          ),
          Row(
            children: [
              Expanded(
                  child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Container(
                    padding: const EdgeInsets.only(bottom: 8),
                    child: Text(
                      'สลัดฟักทองย่างเคลและถั่วเลนทิล',
                      style: TextStyle(fontWeight: FontWeight.bold),
                    ),
                  ),
                  Text(
                    'From Knor',
                    style: TextStyle(color: Colors.grey[500]),
                  )
                ],
              )),
              Icon(
                Icons.star_border_purple500,
                color: Colors.red[600],
              ),
              Text('46')
            ],
          ),
          Text(
        'วัตถุดิบ ฟักทองหั่นชิ้น 1 ถ้วย (125 กรัม) น้ำมันมะกอก 1 ช้อนโต๊ะ (15 กรัม) เคลหั่นชิ้น 1 ถ้วย (50 กรัม) มะเขือเทศเชอร์รี่หั่นครึ่ง 5 ลูก (50 กรัม) ถั่วเลนทิลต้มสุก 3 ช้อนโต๊ะ (25 กรัม) น้ำส้มสายชูบัลซามิก 1 ช้อนโต๊ะ (15 กรัม) น้ำตาลทราย ½ ช้อนโต๊ะ (5 กรัม) น้ำมันมะกอก 2 ช้อนโต๊ะ (28 กรัม) พริกไทยดำบดใหม่ 1/8 ช้อนชา (1 กรัม) คนอร์อร่อยชัวร์รสไก่ 2 ช้อนชา'
        'วิธีทำ ขั้นตอนที่ 1 เคล้าฟักทองกับน้ำมันมะกอกให้ทั่ว นำไปย่างบนกระทะจนสุก ตักใส่จานพักไว้ '
         
        'ขั้นตอนที่ 2 ทำน้ำสลักบัลซามิกโดย ใส่น้ำส้มสายชูบัลซามิก น้ำตาลทราย น้ำมันมะกอก พริกไทยดำ และคนอร์อร่อยชัวร์รสไก่ ลงในอ่างผสม ใช้ตะกร้อมือตีน้ำสลัดให้เข้ากัน พักไว้ '
        
        'ขั้นตอนที่ 3 จัดเสิร์ฟสลัดโดยใส่ฟักทอง เคล มะเขือเทศเชอร์รีและถั่วเลนทิลใส่จาน ราดด้วยน้ำสลัดบัลซามิกให้ทั่ว เสิร์ฟ',
        softWrap: true,
      ),
        ],
      ),
    );
  }
}
