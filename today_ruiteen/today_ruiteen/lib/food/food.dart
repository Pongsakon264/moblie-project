import 'package:flutter/material.dart';
import 'package:today_ruiteen/food/clean1.dart';

class Food extends StatefulWidget {
  const Food({ Key? key }) : super(key: key);

  @override
  _FoodState createState() => _FoodState();
}

class _FoodState extends State<Food> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      
      appBar: AppBar(title: Text("Food Diary"),),
       body: ListView(
          children: [
            Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => clean1()));
                },
                child: Container(
                  width: 150.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://www.knorr.com/content/dam/unilever/global/recipe_image/218/21887/218879-default.jpg/jcr:content/renditions/original'))),
                ),
              ),
            ), 
          ],
        )
    );
  }
}