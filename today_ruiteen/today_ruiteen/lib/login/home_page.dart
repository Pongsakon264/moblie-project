import 'package:flutter/material.dart';
import 'package:today_ruiteen/main.dart';


class HomePage extends StatelessWidget {

  static String tag = 'home-page';

  @override
  Widget build(BuildContext context) {

    final alucard = Hero(
      tag: 'hero',
      child: Padding(
        padding: EdgeInsets.all(16.0),
        child: CircleAvatar(
          radius: 72.0,
          backgroundColor: Colors.green[200],
          backgroundImage: AssetImage('images/running.jpg'),
        ),
      ),
    );

    final welcome = Padding(
        padding: EdgeInsets.all(8.0),
      child: Text(
        'Welcome healthy guys !!',
        style: TextStyle(
          fontSize: 28.0,
          color: Colors.white,
        ),
      ),
    );

    final lorem = Center(
      child: Text(
        'With the new day comes new strengthand new thoughts.',
        style: TextStyle(
          fontSize: 16.0,
          color: Colors.white,
        ),
      ),
    );

   
    
     final button = Container(
       padding: EdgeInsets.all(40),
        child: ElevatedButton(
          
          child: const Text('Next !'),
          onPressed: () {
            Navigator.push(
              context,
              MaterialPageRoute(builder: (context) => MyHomePage(title: 'Today Routine')),
            );
          },
        ),
      );

      final body = Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.all(28.0),
      decoration: BoxDecoration(
        gradient: LinearGradient(
          colors: [
            Colors.green,
            Colors.greenAccent,
          ],
        ),
      ),
      child: Column(
        children: <Widget>[
          alucard, welcome, lorem, button
        ],
      ),
    );
    return Scaffold(
      body: body,
    );
  }
}