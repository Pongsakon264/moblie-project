import 'package:flutter/material.dart';
import 'package:today_ruiteen/exercise/brenchpress.dart';
import 'package:today_ruiteen/exercise/deadlift.dart';
import 'package:today_ruiteen/exercise/shoulderpress.dart';
import 'package:today_ruiteen/exercise/squat.dart';

class exercise extends StatefulWidget {
  const exercise({ Key? key }) : super(key: key);

  @override
  _exerciseState createState() => _exerciseState();
}

class _exerciseState extends State<exercise> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text("Exercise"),backgroundColor: Colors.green[200],
      ),
      body: ListView(
          children: [
            Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ItemScreen1()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/Poom%20Img%2Fbrenchpress.jpg?alt=media&token=cf85b9d1-5812-4554-a3fc-0194324f19be'))),
                ),
              ),
            ),
            Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => ItemScreen2()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/Poom%20Img%2Fshoulder.jpg?alt=media&token=99c65225-8c45-41a4-8f11-c769cad64ba9'))),
                ),
              ),
            ),
             Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => ItemScreen3()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/Poom%20Img%2Fdeadlift.jpg?alt=media&token=047eb2ba-57c4-46d5-850d-ce7f3ff8cbae'))),
                ),
              ),
            ),
             Card(
              child: new InkWell(
                onTap: () {
                  Navigator.push(
                      context, MaterialPageRoute(builder: (context) => ItemScreen4()));
                },
                child: Container(
                  width: 300.0,
                  height: 250.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(15.0),
                      image: DecorationImage(
                          fit: BoxFit.cover,
                          image: NetworkImage(
                              'https://firebasestorage.googleapis.com/v0/b/projectfinal-586b7.appspot.com/o/Poom%20Img%2Fsquat.jpg?alt=media&token=09c38db7-d78f-420f-9f6a-000bb13b7a50'))),
                ),
              ),
            )
          ],
        ));
  }
}

